package de.uni.leipzig.rest;

import groovy.json.JsonSlurper
import groovy.swing.SwingBuilder

import javax.swing.JOptionPane

class TableDialog {
	
	void showDialog(model) {
		def swing = new SwingBuilder()
		
		def frame = swing.frame(title:'Groovy TableModel Demo', location:[200,200], size:[300,200]) {
			panel {
				borderLayout()
				scrollPane(constraints:CENTER) {
					table() {
						tableModel(list:model) {
							closureColumn(header:'Key', read: {row -> row.key})
							closureColumn(header:'Value', read:{row -> row.value})
						}
					}
				}
			}
		}
		frame.setVisible(true)
	}
	
}

class RestClient {
	
	static def restRequest(cityName){
		try {
			def json = URI.create("https://restcountries.eu/rest/v1/capital/${cityName}").toURL().getText()
			def slurper = new JsonSlurper()
			def result = slurper.parseText(json)[0]
							    .collect({k,v -> [key : k, value : v]})
			new TableDialog().showDialog(result)
		} catch(FileNotFoundException e){
			JOptionPane.showMessageDialog(null, 'There is no city with the name: ' + cityName)
		}
	}
	
	static def run(){
		def input = JOptionPane.showInputDialog(null, 'Information zu einer Hauptstadt', "Dialog", JOptionPane.INFORMATION_MESSAGE)
		switch(input){
			case null:
				println('finished program')
				System.exit(0)
				break
			case '':
				JOptionPane.showMessageDialog(null, 'Please provide some city name!')
				break
			case String.class:
				restRequest(input.toLowerCase())
				run()
				break
		}
		
	}
	
	public static void main(String[] args) {
		run()
	}
}
