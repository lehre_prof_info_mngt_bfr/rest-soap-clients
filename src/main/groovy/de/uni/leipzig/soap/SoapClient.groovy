package de.uni.leipzig.soap;

import de.uni.leipzig.IP2Geo
import de.uni.leipzig.ObjectFactory;
import de.uni.leipzig.ResolveIPResponse;

class SoapClient {

	public static void main(String[] args) {
		def local = new IP2Geo()
		println(local.getServiceName())
		def result = local.getIP2GeoSoap().resolveIP("139.18.242.112", "0")
		println("Result typ: ${result.class}")
		//println("Längengrad: ${result.longitude}, Breitengrad: ${result.latitude}")
		println(result.regionName)
	}
}
