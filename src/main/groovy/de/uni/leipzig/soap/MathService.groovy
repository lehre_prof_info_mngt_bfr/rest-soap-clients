package de.uni.leipzig.soap

import groovy.net.soap.SoapServer


class Main {

	static main(args) {
		def server = new SoapServer("localhost", 6980)

		server.setNode("MathService")

		server.start()
	}
}
